﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pong
{
    public class Ball : GameObject
    {
        
        public float speed = 500f;
        public Vector2 direction = new Vector2(0,0);
        private Random rand = new Random();

        public Ball(Game myGame) : base(myGame)
        {
            texture = new Texture2D(myGame.GraphicsDevice, 10, 10);
            Color[] color = new Color[10 * 10];
            for (int i = 0; i < color.Length; i++)
            {
                color[i] = Color.White;
            }
            texture.SetData(color);

            setRandomDirection();
        }

        public override void Update(float deltaTime, GameObject player, GameObject ai)
        {
            position += direction * speed * deltaTime;
            CheckBallCollision(player);
            CheckBallCollision(ai);

            //base.Update(deltaTime);
        }
        private void setRandomDirection()
        {
            direction = Calc2D.GetRightPointingAngledPoint(rand.Next(1, 50));
            if(rand.Next(2) == 1)
            {
                direction = -direction;
            }
        }
        public void CheckBallCollision(GameObject p)
        {
            var ball = this;
            if (ball.BoundingBox.Intersects(p.BoundingBox))
            {
                float differenceToTargetCenter = p.BoundingBox.Center.Y - BoundingBox.Center.Y;

                // Reflect based on which part of the paddle is hit

                // By default, set the normal to "up"
                Vector2 normal = -1.0f * Vector2.UnitY;

                // Distance from the leftmost to rightmost part of the paddle
                float dist = p.Width;
                // Where within this distance the ball is at
                float ballLocation = ball.position.X -
                    (p.position.X);
                // Percent between leftmost and rightmost part of paddle
                float pct = ballLocation / dist;

                if (pct < 0.33f)
                {
                    normal = new Vector2(-0.196f, -0.981f);
                }
                else if (pct > 0.66f)
                {
                    normal = new Vector2(0.196f, -0.981f);
                }

                ball.direction = Vector2.Reflect(ball.direction, normal);
            }
        }
    }
}
