﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pong
{
    class Paddle : GameObject
    {
        public float speed = 500f;
        public Vector2 direction = new Vector2(0, 0);
        private Random skipFrams = new Random();
        

        public Paddle(Game myGame) : base(myGame)
        {
            texture = new Texture2D(myGame.GraphicsDevice, 100, 20);
            Color[] playerColor = new Color[100 * 20];
            for (int i = 0; i < playerColor.Length; i++)
            {
                playerColor[i] = Color.Blue;
            }
            texture.SetData(playerColor);
            //position = new Vector2(myGame.GraphicsDevice.Viewport.Width / 2 - 50, myGame.GraphicsDevice.Viewport.Height - 60);
        }

        public override void Update(float deltaTime, GraphicsDevice g)
        {
            KeyboardState keyState = Keyboard.GetState();
            if (keyState.IsKeyDown(Keys.Right))
                position.X += 500 * deltaTime;
            if (keyState.IsKeyDown(Keys.Left))
                position.X -= 500 * deltaTime;
            position.X = MathHelper.Clamp(position.X, 0, g.Viewport.Width - texture.Width);

        }
    }
}
