﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pong
{
    class AI : GameObject
    {
        public float speed = 500f;
        private Random skipFrams = new Random();


        public AI(Game myGame) : base(myGame)
        {
            texture = new Texture2D(myGame.GraphicsDevice, 100, 20);
            Color[] playerColor = new Color[100 * 20];
            for (int i = 0; i < playerColor.Length; i++)
            {
                playerColor[i] = Color.IndianRed;
            }
            texture.SetData(playerColor);
            //position = new Vector2(myGame.GraphicsDevice.Viewport.Width / 2 - 50, myGame.GraphicsDevice.Viewport.Height - 60);
        }

        public override void Update(float deltaTime, GraphicsDevice g, GameObject gameObject)
        {
            if (position.X < gameObject.position.X)
            {
                if (skipFrams.Next(1, 100) < 92)
                {
                    position.X += 500 * deltaTime;
                }
            }
            else if (position.X > gameObject.position.Y)
            {
                if (skipFrams.Next(1, 100) < 92)
                {
                    position.X -= 500 * deltaTime;
                }
            }
            position.X = MathHelper.Clamp(position.X, 0, g.Viewport.Width - 100);

        }
    }
}
