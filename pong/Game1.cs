﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace pong
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        //Texture2D playerTexture;
        //Texture2D aiTexture;
        Vector2 playerPosition;
        Vector2 aiPosition;
        Ball ball;
        Paddle player;
        AI enemy;

        int ballWithPaddle = 0;
        private int playerScore = 0;
        private int aiScore = 0;
        private Random skipFrams = new Random();
        //private Rectangle playerBoundingBox;
        private Rectangle aiBoundingBox;
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            //aiPosition = new Vector2(this.GraphicsDevice.Viewport.Width / 2- 50, 40);
            //playerPosition = new Vector2(this.GraphicsDevice.Viewport.Width / 2 - 50, this.GraphicsDevice.Viewport.Height - 60);
            
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            ball = new Ball(this);
            ball.LoadContent();
            ball.position = new Vector2(this.GraphicsDevice.Viewport.Width / 2 , this.GraphicsDevice.Viewport.Height / 2 );
            player = new Paddle(this);
            player.position = new Vector2(this.GraphicsDevice.Viewport.Width / 2 - player.Width /2, this.GraphicsDevice.Viewport.Height - 60);
            enemy = new AI(this);
            enemy.position = new Vector2(this.GraphicsDevice.Viewport.Width / 2 - enemy.Width / 2, 40);

        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            float deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            player.Update(deltaTime, this.GraphicsDevice);
            ball.Update(deltaTime, player, enemy);
            enemy.Update(deltaTime, this.GraphicsDevice, ball);
            CheckCollisions();

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            // TODO: Add your drawing code here
            spriteBatch.Begin();
            
            //spriteBatch.Draw(aiTexture, aiPosition);
            player.Draw(spriteBatch);
            ball.Draw(spriteBatch);
            enemy.Draw(spriteBatch);
            spriteBatch.End();

            base.Draw(gameTime);
        }
        
        public void CheckCollisions()
        {
            float radius = ball.Width / 2;

            if (Math.Abs(ball.position.X) < radius)
            {
                ball.direction.X = -1f * ball.direction.X;
            }
            else if(Math.Abs(ball.position.X - this.GraphicsDevice.Viewport.Width) < radius)
            {
                ball.direction.X = -1f * ball.direction.X;
            }
            if(Math.Abs(ball.position.Y) < radius)
            {
                AIPoint(); 
            }
            else if(Math.Abs(ball.position.Y - this.GraphicsDevice.Viewport.Height) < radius)
            {
                PlayerPoint();
            }

        }

        public void AIPoint()
        {
            aiScore += 1;
            Reset();
        }

        public void PlayerPoint()
        {
            playerScore += 1;
            Reset();
        }

        public void Reset()
        {
            ball.position = new Vector2(this.GraphicsDevice.Viewport.Width / 2, this.GraphicsDevice.Viewport.Height / 2);
            enemy.position = new Vector2(this.GraphicsDevice.Viewport.Width / 2 - enemy.Width /2 , 40);
            player.position = new Vector2(this.GraphicsDevice.Viewport.Width / 2 - player.Width /2, this.GraphicsDevice.Viewport.Height - 60);

        }
    }
}